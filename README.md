Syntax checking Git hooks
=========================

As used by Tom Ryder, because Michael Fincham asked.

The `pre-commit` file goes in `.git/hooks` in your repository, and you make it
executable. You'll need `file(1)` and various other binaries in your `PATH`.

This is nowhere near as tidy as it could be, but it correctly handles syntax
checking and linting of added or changed files only, without rescanning the
whole project. It does this with some byzantine Bash. It handles filenames with
funny characters (including newlines) correctly.

I'll write some real documentation Later™.

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under [MIT License][2].

[1]: https://sanctum.geek.nz/
[2]: http://opensource.org/licenses/MIT
